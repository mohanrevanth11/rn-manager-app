import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import LoginForm from './components/LoginForm';

class App extends Component{

    componentWillMount(){

        const Firebase = require("firebase");

        const config = {
            apiKey: 'AIzaSyBsu946bB6JU6bZaG36cgm7y_RU6wSLVvY',
            authDomain: 'manager-app-774b2.firebaseapp.com',
            databaseURL: 'https://manager-app-774b2.firebaseio.com',
            projectId: 'manager-app-774b2',
            storageBucket: 'manager-app-774b2.appspot.com',
            messagingSenderId: '728970853573'
          };

          Firebase.initializeApp(config);
    }


    render(){
        const store = createStore (reducers, {}, applyMiddleware(ReduxThunk));
        return(
            <Provider store = {store}>              
                <LoginForm/>
            </Provider>
        );
    }
}

export default App;