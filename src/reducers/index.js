import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';

export default combineReducers({
    //Key is the property of the state
    auth: AuthReducer
});