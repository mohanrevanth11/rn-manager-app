import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Card, CardSection, Input, Button, Spinner} from './common';
import {connect} from 'react-redux';
import {emailChanged, passwordChanged, loginUser} from '../actions';

class LoginForm extends Component{

    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);             
    }

    onButtonPress(){
        const {email, password} = this.props;
        this.props.loginUser({email, password});
    }

    renderError(){
        if(this.props.error){
            return(
                <View style= {{backgroundColor: 'white'}}>
                    <Text style= {styles.errorTextStyle}>
                        {this.props.error}
                    </Text>

                </View>
            )
        }
    }

    renderButton(){
        if(this.props.loading){
           return <Spinner size="large" />; 
        }
        return(
        <Button onPress = {this.onButtonPress.bind(this)}>
            <Text style={styles.textStyle}>Login</Text>            
        </Button>
        );
    }


    render(){
        return(
            <View style ={{backgroundColor: '#FFF',flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <CardSection>
                    <Input 
                        label="Email"
                        placeHolder = "abc@xyz.com"
                        onChangeText = {this.onEmailChange.bind(this)}
                        value={this.props.email}
                    />
                </CardSection>

                <CardSection>
                    <Input
                        label = "Password"
                        secureTextEntry
                        placeHolder = "password"
                        onChangeText = {this.onPasswordChange.bind(this)}
                        value = {this.props.password}
                    />                    
                </CardSection>

                {this.renderError()}

                <CardSection>                                        
                    {this.renderButton()}
                </CardSection>                
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password,
        error: state.auth.error,
        loading: state.auth.loading
    }
}

export default connect (mapStateToProps, {
    emailChanged, passwordChanged, loginUser
})(LoginForm);

const styles = StyleSheet.create({
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    textStyle: {    
        color: '#FFF',
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        width: 50,
        fontWeight: 'bold'
      },
});