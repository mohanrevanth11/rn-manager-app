import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

const Button = ({ onPress, children }) => {  
  return (
    <TouchableOpacity onPress={onPress} style={styles.buttonStyle}>
        {children}
    </TouchableOpacity>
  );
};



const styles = StyleSheet.create({  
  buttonStyle: {
    flex: 1,    
    backgroundColor: '#303F9F',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#FFF',
    marginLeft: 5,
    marginRight: 5
  }
});

export {Button};